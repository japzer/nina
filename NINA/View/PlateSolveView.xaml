﻿<!--
    Copyright © 2016 - 2019 Stefan Berg <isbeorn86+NINA@googlemail.com>

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    N.I.N.A. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    N.I.N.A. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with N.I.N.A..  If not, see http://www.gnu.org/licenses/.-->
<UserControl
    x:Class="NINA.View.PlateSolveView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:local="clr-namespace:NINA.View"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:ninactrl="clr-namespace:NINACustomControlLibrary;assembly=NINACustomControlLibrary"
    xmlns:ns="clr-namespace:NINA.Locale"
    d:DesignHeight="300"
    d:DesignWidth="300"
    mc:Ignorable="d">

    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition />
            <RowDefinition />
        </Grid.RowDefinitions>
        <Image
            MaxWidth="300"
            MaxHeight="300"
            Source="{Binding Thumbnail}"
            Stretch="Uniform" />
        <ninactrl:LoadingControl
            Width="40"
            Height="40"
            HorizontalAlignment="Center"
            VerticalAlignment="Center"
            Visibility="{Binding PlateSolveResult, Converter={StaticResource InverseNullToVisibilityConverter}}" />
        <StackPanel Grid.Row="1" Orientation="Vertical">
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblCenterRA}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Coordinates.RA, StringFormat=\{0:0.000\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblCenterRAhms}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Coordinates.RAString}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblCenterDec}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Coordinates.Dec, StringFormat=\{0:0.000\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblCenterDecdms}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Coordinates.DecString}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblRadius}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Radius, StringFormat=\{0:0.000 deg\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblPixelScale}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Pixscale, StringFormat=\{0:0.00 arcsec/pixel\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblOrientation}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Orientation, StringFormat=\{0:0.00\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblEpoch}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Coordinates.Epoch}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblErrorDistance}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.Separation.Distance, StringFormat=\{0:0.00°\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblRAError}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.RaErrorString}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblRAPixError}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.RaPixError, StringFormat=\{0:0.00\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblDecError}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.DecErrorString}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblDecPixError}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding PlateSolveResult.DecPixError, StringFormat=\{0:0.00\}}" />
                </UniformGrid>
            </Border>
        </StackPanel>
    </Grid>
</UserControl>